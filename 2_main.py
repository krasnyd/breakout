# https://gym.openai.com/envs/Breakout-v0/
import random
from collections import deque

import gym
import numpy as np
from tensorflow.keras import layers
from tensorflow.python.framework.ops import disable_eager_execution
from tensorflow.python.keras.models import load_model, Sequential
from tensorflow.python.keras.optimizer_v2.adam import Adam

# To fix tf2 speed - https://github.com/tensorflow/tensorflow/issues/33024
disable_eager_execution()


# https://stable-baselines3.readthedocs.io/en/master/_modules/stable_baselines3/common/atari_wrappers.html#AtariWrapper
class MaxAndSkipEnv(gym.Wrapper):
    """
    Return only every ``skip``-th frame (frameskipping)

    :param env: the environment
    :param skip: number of ``skip``-th frame
    """

    def __init__(self, env: gym.Env, skip: int = 4):
        gym.Wrapper.__init__(self, env)
        # most recent raw observations (for max pooling across time steps)
        self._obs_buffer = np.zeros((2,) + env.observation_space.shape, dtype=env.observation_space.dtype)
        self._skip = skip

    def step(self, action: int):
        """
        Step the environment with the given action
        Repeat action, sum reward, and max over last observations.

        :param action: the action
        :return: observation, reward, done, information
        """
        total_reward = 0.0
        done = None
        for i in range(self._skip):
            obs, reward, done, info = self.env.step(action)
            if i == self._skip - 2:
                self._obs_buffer[0] = obs
            if i == self._skip - 1:
                self._obs_buffer[1] = obs
            total_reward += reward
            if done:
                break
        # Note that the observation on the done=True frame
        # doesn't matter
        max_frame = self._obs_buffer.max(axis=0)

        return max_frame, total_reward, done, info

    def reset(self, **kwargs):
        return self.env.reset(**kwargs)

env = gym.make("Breakout-ram-v0")
env = MaxAndSkipEnv(env, skip=4)

class DDQNAgent:
    def __init__(self, env):
        self.action_size = env.action_space.n  # 4 actions (0, 1, 2, 3)
        self.state_dim = env.observation_space.shape

        self.learning_rate = 0.001
        self.gamma = 0.99
        self.epsilon = 1.0
        self.epsilon_decay = 0.9995
        self.epsilon_min = 0.1
        self.memory = deque(maxlen=200_000)

        self.env = env
        self.batch_size = 64

        self.model = self.build_model()

    def build_model(self):
        model = Sequential([
            layers.Dense(256, input_shape=self.state_dim, activation="relu"),
            layers.Dense(512, activation="relu"),
            layers.Dense(256, activation="relu"),
            layers.Dense(128, activation="relu"),
            layers.Dense(self.action_size, activation=None)
        ])
        model.compile(loss="mean_squared_error", optimizer=Adam(lr=self.learning_rate))
        model.summary(line_length=120)
        return model

    def get_action(self, state, use_random=True):
        if use_random and np.random.random() < self.epsilon:
            return random.randrange(self.action_size)
        else:
            q_state = self.model.predict(np.array([state]))
            return np.argmax(q_state)  # 0, 1, 2, 3

    def remember(self, state, action, reward, next_state, done):
        experience = state, action, reward, next_state, done
        self.memory.append(experience)

    def replay(self):
        if len(self.memory) < self.batch_size:
            return
        # Randomly sample minibatch from the memory
        minibatch = random.sample(self.memory, self.batch_size)

        state = np.zeros((self.batch_size, *self.state_dim))
        next_state = np.zeros((self.batch_size, *self.state_dim))
        action, reward, done = [], [], []

        # do this before prediction
        # for speedup, this could be done on the tensor level
        # but easier to understand using a loop
        for i in range(self.batch_size):
            state[i] = minibatch[i][0]
            action.append(minibatch[i][1])
            reward.append(minibatch[i][2])
            next_state[i] = minibatch[i][3]
            done.append(minibatch[i][4])

        # do batch prediction to save speed
        target = self.model.predict(state)
        target_next = self.model.predict(next_state)

        for i in range(self.batch_size):
            # correction on the Q value for the action used
            if done[i]:
                target[i][action[i]] = reward[i]
            else:
                # Standard - DQN
                # DQN chooses the max Q value among next actions
                # selection and evaluation of action is on the target Q Network
                # Q_max = max_a' Q_target(s', a')
                target[i][action[i]] = reward[i] + self.gamma * (np.amax(target_next[i]))

        # Train the Neural Network with batches
        self.model.fit(state, target, batch_size=self.batch_size, verbose=0)

    def load(self, name):
        self.model = load_model(name)

    def save(self, name):
        self.model.save(name)

    def run(self):
        decay_step = 0

        # Each of this episode is its own game.
        for episode in range(200_0000):
            state = self.env.reset() / 255  # normalize
            # this is each frame, up to 500...but we wont make it that far with random.
            sum_reward = 0
            for t in range(10_000):
                decay_step += 1
                # self.env.render()
                # sleep(0.1)

                action = self.get_action(state)

                next_state, reward, done, info = self.env.step(action)
                next_state = next_state / 255  # normalize


                sum_reward += reward
                self.remember(state, action, reward, next_state, done)
                state = next_state

                if done:
                    break

                if decay_step % 4 == 0:
                    self.replay()

            agent.epsilon = max(agent.epsilon_min, agent.epsilon * agent.epsilon_decay)

            if episode % 1000 == 0:
                self.save(f"breakout-{episode:08}.h5")

            print(f"e: {episode:07}, eps: {agent.epsilon:.2f}, r: {sum_reward}")

if __name__ == "__main__":
    agent = DDQNAgent(env)
    agent.run()
