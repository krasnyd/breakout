# https://gym.openai.com/envs/Breakout-v0/
import random
from collections import deque

import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.python.framework.ops import disable_eager_execution
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.optimizer_v2.adam import Adam

# To fix tf2 speed - https://github.com/tensorflow/tensorflow/issues/33024
from baselines_wrapers import make_atari, wrap_deepmind

disable_eager_execution()

# Use the Baseline Atari environment because of Deepmind helper functions
env = make_atari("BreakoutNoFrameskip-v4")
# Warp the frames, grey scale, stake four frame and scale to smaller ratio
env = wrap_deepmind(env, frame_stack=True, scale=True)
env.seed(43)


class DDQNAgent:
    def __init__(self, env):
        self.action_size = env.action_space.n  # 4 actions (0, 1, 2, 3)
        self.state_dim = env.observation_space.shape  # (84, 84, 4)

        self.learning_rate = 0.003
        self.gamma = 0.99
        self.epsilon = 1.0
        self.epsilon_decay = 0.9998
        self.epsilon_min = 0.1
        self.memory = deque(maxlen=10_000)
        self.env = env

        self.batch_size = 32

        self.model = self.build_model()

    def build_model(self):
        inputs = layers.Input(shape=self.state_dim)

        # Convolutions on the frames on the screen
        # https://www.researchgate.net/figure/ReLU-activation-function_fig7_333411007
        layer1 = layers.Conv2D(32, 8, strides=4, activation="relu")(inputs)
        layer2 = layers.Conv2D(64, 4, strides=2, activation="relu")(layer1)
        layer3 = layers.Conv2D(64, 3, strides=1, activation="relu")(layer2)

        layer4 = layers.Flatten()(layer3)

        layer5 = layers.Dense(512, activation="relu")(layer4)
        action = layers.Dense(self.action_size, activation="linear")(layer5)

        model = keras.Model(inputs=inputs, outputs=action)
        model.compile(loss="mean_squared_error", optimizer=Adam(learning_rate=0.00025, clipnorm=1.0))
        model.summary(line_length=120)
        return model

    def get_action(self, state, use_random=True):
        if use_random and np.random.random() <= self.epsilon:
            return random.randrange(self.action_size)
        else:
            q_state = self.model.predict(np.array([state]))
            return np.argmax(q_state)  # 0, 1, 2, 3

    def remember(self, state, action, reward, next_state, done):
        experience = state, action, reward, next_state, done
        self.memory.append(experience)

    def replay(self):
        if len(self.memory) < self.batch_size:
            return

        # Randomly sample minibatch from the memory
        minibatch = random.sample(self.memory, self.batch_size)

        state = np.zeros((self.batch_size, *self.state_dim))
        next_state = np.zeros((self.batch_size, *self.state_dim))
        action, reward, done = [], [], []

        # do this before prediction
        # for speedup, this could be done on the tensor level
        # but easier to understand using a loop
        for i in range(self.batch_size):
            state[i] = minibatch[i][0]
            action.append(minibatch[i][1])
            reward.append(minibatch[i][2])
            next_state[i] = minibatch[i][3]
            done.append(minibatch[i][4])

        # do batch prediction to save speed
        target = self.model.predict(state)
        target_next = self.model.predict(next_state)

        for i in range(self.batch_size):
            # correction on the Q value for the action used
            if done[i]:
                target[i][action[i]] = reward[i]
            else:
                # Standard - DQN
                # DQN chooses the max Q value among next actions
                # selection and evaluation of action is on the target Q Network
                # Q_max = max_a' Q_target(s', a')
                target[i][action[i]] = reward[i] + self.gamma * (np.amax(target_next[i]))

        # Train the Neural Network with batches
        self.model.fit(state, target, batch_size=self.batch_size, verbose=0)

    def load(self, name):
        self.model = load_model(name)

    def save(self, name):
        self.model.save(name)

    # def sakra_ten_state_chceme_spravne(self, image_state):
    #     next_state = cv2.cvtColor(image_state, cv2.COLOR_RGB2GRAY)
    #     next_state = cv2.threshold(next_state, 1, 255, cv2.THRESH_BINARY)[1]
    #     next_state = next_state[32:-18, 8:-8]
    #     # to_print = cv2.resize(next_state, (next_state.shape[1]*2, next_state.shape[0]*2))
    #     next_state = np.atleast_3d(next_state)
    #     # cv2.imshow("JAHODA", to_print)
    #     # cv2.waitKey(0)
    #     return next_state

    def run(self):
        STEP_TO_LOAD = 0
        # self.epsilon = self.epsilon * (self.epsilon_decay ** STEP_TO_LOAD)
        # self.load(f"breakout-{STEP_TO_LOAD:08}.h5")
        decay_step = 0
        episode_reward_history = deque(maxlen=10000)

        # Each of this episode is its own game.
        for episode in range(STEP_TO_LOAD+1, 1000000000):
            state = np.array(self.env.reset())            # this is each frame, up to 500...but we wont make it that far with random.
            sum_reward = 0
            for t in range(10_000):
                decay_step += 1
                self.env.render()

                action = self.get_action(state)
                next_state, reward, done, info = self.env.step(action)
                next_state = np.array(next_state)

                # cv2.imshow("JAHODA", next_state[:, :, 1])
                # cv2.waitKey(0)

                sum_reward += reward
                self.remember(state, action, reward, next_state, done)
                state = next_state

                if done:
                    break

                if decay_step % 4 == 0:
                    self.replay()

            agent.epsilon = max(agent.epsilon_min, agent.epsilon * agent.epsilon_decay)

            if episode % 500 == 0:
                self.save(f"breakout-{episode:08}.h5")

            episode_reward_history.append(sum_reward)
            running_reward = sum(episode_reward_history) / len(episode_reward_history)
            print(f"e: {episode:03}, eps: {agent.epsilon:.2f}, run_rew: {running_reward:.2f}")

if __name__ == "__main__":
    agent = DDQNAgent(env)
    agent.run()

# while True:
#     print("lol")
#     state = env.reset()
#     done = False
#     while not done:
#         action = env.action_space.sample()
#         state, reward, done, _ = env.step(action)
#         env.render()
#         sleep(0.05)
